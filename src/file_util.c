#include "file_util.h"

void append_new_path(char *cache,
                     char *working,
                     const char *new_path) {
  if (cache[strlen(cache)-1] != '/') {
    strcat(cache, "/");
  }
  if (working[strlen(working)-1] != '/') {
    strcat(working, "/");
  }
  strcat(cache, new_path);
  strcat(working, new_path);
}

void clear_appended_path(char *cache, char *working, int working_path_len, int cache_path_len) {
  working[working_path_len] = '\0';
  cache[cache_path_len] = '\0';
}

bool check_dir_error(DIR *dir, const char *dir_name) {
  if (dir == NULL) {
    fprintf(stderr, "When opening %s\n", dir_name);
    perror("WARNING");
    clear_error();
    return true;
  }
  return false;
}

void scan_file(const char *working_dir_name, 
               const char *cache_dir_name, 
               int (*file_process)(const char *, const char *),
               int working_path_len,
               int cache_path_len) {
  static char cur_working_path[MAX_PATH_LEN];
  static char cur_cache_path[MAX_PATH_LEN];

  if (working_dir_name != NULL) {
    working_path_len = strlen(working_dir_name);
    strcpy(cur_working_path, working_dir_name);
  }
  if (cache_dir_name != NULL) {
    cache_path_len = strlen(cache_dir_name);
    strcpy(cur_cache_path, cache_dir_name);
  }

  clear_error(); 
  DIR *working_dir;
  working_dir = opendir(cur_working_path);
  if(check_dir_error(working_dir, cur_working_path)) {
    return;
  }
  
  struct dirent *dir_item = NULL;
  while ((dir_item = readdir(working_dir)) != NULL) {
    if (dir_item->d_name[0] == '.') {
      continue;
    }

    if (dir_item->d_type == DT_DIR) {
      append_new_path(cur_cache_path, cur_working_path, dir_item->d_name);
      mkdir(cur_cache_path, 0777);
      scan_file(NULL, NULL,
                    file_process,
                    strlen(cur_working_path),
                    strlen(cur_cache_path));
      
      clear_appended_path(cur_cache_path, cur_working_path, working_path_len, cache_path_len);
    } else if (dir_item->d_type == DT_REG) {
      append_new_path(cur_cache_path, cur_working_path, dir_item->d_name);
      (*file_process)(cur_working_path, cur_cache_path);
      clear_appended_path(cur_cache_path, cur_working_path, working_path_len, cache_path_len);
    }
  }
  clear_error(); 
  closedir(working_dir);
  check_error();
}

