#ifndef FILE_UTIL_H
#define FILE_UTIL_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <dirent.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "misc.h"
#define MAX_PATH_LEN 1000

void scan_file(const char *working_dir_name, 
                    const char *cache_dir_name, 
                    int (*file_process)(const char *, const char *),
                    int ,int);

#endif
