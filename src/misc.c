#include "misc.h"

extern int errno;

void check_error() {
  if (errno != 0) {
    perror("ERROR");
    exit(EXIT_FAILURE);
  }
}

void clear_error() {
  errno = 0;
}
